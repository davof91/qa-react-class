import React from 'react';

class MyClassComponent extends React.Component {
    render(){
        return (
            <>
                <h2>Holla Holla</h2>
                <p>Testing text</p>
            </>
        )
    }
};

export default MyClassComponent;