import React from 'react';
import { create } from 'react-test-renderer';
import AllTodos from '../Components/Alltodos';
import sampleTodos from '../sampleTodos.json';

test('it should render the correct number of Todo components based on the todo array supplied', () => {
    const allTodos = create(<AllTodos data={{todos:sampleTodos}}/>);

    const sampleTodosLength = sampleTodos.length;

    const testInstance = allTodos.root;

    const tableBody = testInstance.findByType('tbody');
    
    expect(tableBody.children.length).toEqual(sampleTodosLength);
}); 