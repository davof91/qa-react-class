import React from 'react';
import { create, act } from 'react-test-renderer';
import TodoForm from '../Components/TodoForm'


jest.mock('../Components/utils/DateCreated', ()=>{
    return function MockDateCreated(){
        return <span testid="dateCreated">Date Created Component</span>
    }
})

describe("TodoForm test suite", () =>{
    describe("DateCreated function and render tests", () => {
        test("Testing dates",() => {
            const testRenderer = create(<TodoForm />);
            const testInstance = testRenderer.root;
            const dateCreated = testInstance.find(el => el.type === 'span' &&  el.props.testid === 'dateCreated');
            expect(dateCreated).toBeTruthy();
            expect(dateCreated.children).toContain(`Date Created Component`)
        })
    })
});

test('it should render the new value in the input when todoDescription onChange is activated', () => {
    const testValue = "Test";
    const testRenderer = create( <TodoForm />)

    const testInstance = testRenderer.root;

    const descInput = testInstance.findByProps({name: "todoDesctiption"});

    expect(descInput.props.value).toBe("");

    act(() => descInput.props.onChange({target: {value: 'testValue'}}));

    expect(descInput.props.value).toBe("testValue");
})


test('it should render the new value in the input when todoCompleted onChange is activated', () => {
    const testValue = "Test";
    const testRenderer = create( <TodoForm />)

    const testInstance = testRenderer.root;

    const descInput = testInstance.findByProps({name: "todoCompleted"});

    expect(descInput.props.checked).toBeFalsy();

    act(() => descInput.props.onChange({target: {checked: true}}));

    expect(descInput.props.checked).toBeTruthy();
})

test('it should render the new value for disabled button', () => {
    const testValue = "Test";
    const testRenderer = create( <TodoForm />)

    const testInstance = testRenderer.root;

    const descInput = testInstance.findByProps({name: "todoDesctiption"}); 

    const submitBtn = testInstance.findByProps({type: `submit`}); 
    
    expect(submitBtn.props.disabled).toBeTruthy();
    
    act(() => descInput.props.onChange({ target: { value: testValue } }));

    expect(submitBtn.props.disabled).toBeFalsy(); 
    
    expect(submitBtn.props.className).toContain(`btn-primary`);
})