import React from 'react';
import { create } from 'react-test-renderer';

import ComponentWithProps from '../ComponentWithProps';

test("it should render the correct heading from default", () =>{
    const component = create(<ComponentWithProps />);
    const testHeader = component.root.findByType("h1");

    expect(testHeader.children).toContain('Header from defaults');

});

test("it should render the correct heading from props when a header prop is supplied", () =>{
    const testHeaderString = "Test Header"
    const component = create(<ComponentWithProps header={testHeaderString}/>);
    const testHeader = component.root.findByType("h1");

    expect(testHeader.children).toContain(testHeaderString);

});

const component = create(<ComponentWithProps />);
    const testHeader = component.root.findByType("h1");

    expect(testHeader.children).toContain('Header from defaults');

test("it should render the correct content from defaults", () =>{
    const component = create(<ComponentWithProps />);
    const testContent = component.root.findAllByType("p");

    expect(testContent[0].children).toContain('Contents from defaults');

});

test("it should render the correct content from props when a content prop is supplied", () =>{
    const testHeaderString = "Test Content"
    const component = create(<ComponentWithProps content={testHeaderString}/>);
    const testHeader = component.root.findAllByType("p");

    expect(testHeader[0].children).toContain(testHeaderString);

});

test(" it should render the correct number from props when a number prop is supplied", () =>{
    const testHeaderString = 20
    const component = create(<ComponentWithProps number={testHeaderString}/>);
    const testHeader = component.root.findAllByType("p");

    expect(testHeader[1].children).toContain(testHeaderString.toString());

});