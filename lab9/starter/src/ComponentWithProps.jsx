import React from 'react';
import PropTypes from 'prop-types';

const ComponentWithProps = props => {
    return (
        <>
            <h1>{ props.header }</h1>
            <p> { props.content }</p>
            <p> Number: { props.number } </p>
            <p> Prop that doesn't exist:{ props.nonexistent } </p>
        </>
    );
};

ComponentWithProps.defaultProps = {
    'header':"Header from defaults",
    'content':"Contents from defaults",
    'number': 100
};

ComponentWithProps.propTypes = {
    'header':PropTypes.string.isRequired,
    'content':PropTypes.string.isRequired,
    'number': PropTypes.number.isRequired
};

export default ComponentWithProps;