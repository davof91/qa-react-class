# QA React Class
This repo will contain all the code for the different labs and project been built throughout the class. Yes the code is repetitive through each lab because we are building on top of each version. Below you will find a run down of each lab and what each was needed to be done.

## Labs
Short description per lab to know what the differences are.

### Lab1
Creating a project with the npx command 'npx create-react-app starter'

### Lab2
It was just build to see the build command so we dont have a project

### Lab3
Building the first 2 components for a page using functional programing

### Lab4
Building another component for a page as a class

### Lab5
There was no lab, it was just talking about testing. This will be covered on Lab7b

### Lab6
Make a mockup of what the app should look like. Just made an elements.txt file with my structure

### Lab7a
Building the footer and the header of the app.

### Lab7b
Building testing for the project using react-test-render

### Lab8 
Look at props. Lab4 starter used for this one.

### Lab9
Doing testing on the props. Follows lab8

### Lab10 (a & b)
This lab has use doing the todo list in the lab 6. Also covers the testing.

### Lab10 (c & d)
Adding a form and testing it.

### Lab11 and 12
Think about the state and how it should work.

### Lab13 (a & b)
Adding the state of the data to the application and testing.

### Lab14 (a & b)
Adding inverse data flow and testing it.