import React, { useState } from 'react';
import DateCreated from './utils/DateCreated'
import PropTypes from 'prop-types';

const TodoForm = (props) =>{
    const [todoDescription, setTodoDescription] = useState("");
    const [todoDateCreated, setTodoDateCreated] = useState(null);
    const [todoCompleted, setCompleted] = useState(false);

    const handleSubmit  = event => {
        event.preventDefault();
        props.submitTodo(todoDescription, todoDateCreated, todoCompleted);
        setTodoDescription("");
        setTodoDateCreated(null);
        setCompleted(false);
    }

    return (
        <form onSubmit={ handleSubmit }>
            <div className="form-group">
                <label htmlFor="todoDesctiption">Description:&nbsp;</label>
                <input type="text" name="todoDesctiption" className="form-control" placeholder="Todo Description"
                     value={todoDescription} onChange={(e)=>{ setTodoDescription(e.target.value) }}></input>
            </div>
            <div className="form-group">
                <label htmlFor="todoDateCreated">Created on:&nbsp;</label>
                <DateCreated updateDateCreated={(dateCreated)=>{ setTodoDateCreated(dateCreated) }}/>
            </div>
            <div className="form-group">
                <label htmlFor="todoCompleted">Completed:&nbsp;</label>
                <input type="checkbox" name="todoCompleted" checked={ todoCompleted }
                        onChange={(e)=>{ setCompleted(e.target.checked) }} ></input>
            </div>
            <div className="form-group">
                 <input type="submit" value="Submit" className="btn btn-primary" disabled={todoDescription == ""}
                        className={todoDescription != "" ? "btn-primary" : "btn-danger"} />
            </div>
        </form>
    )
};

TodoForm.propTypes = {
    'submitTodo': PropTypes.func.isRequired 
};

export default TodoForm;